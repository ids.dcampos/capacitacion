import { html, css, LitElement } from 'lit';

export class LitelementsTemplates extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        padding: 25px;
        color: var(--litelements-templates-text-color, #000);
      }
    `;
  }

  static get properties() {
    return {
      title: { type: String },
      counter: { type: Number },

      myString: { type: String},
      myArray: { type: Array},
      myBool: { type: Boolean}

    };
  }

  constructor() {
    super();
    this.title = 'Hey there';
    this.counter = 5;

    this.myString = 'Hello world';
    this.myArray = ['an','array','of','test','data'];
    this.myBool = true;
  }

  __increment() {
    this.counter += 1;
  }

  render() {
    return html`
      <h2>${this.title} Nr. ${this.counter}!</h2>
      <button @click=${this.__increment}>increment</button>
      <hr>
      <p>array loops and conditionals.
      <p>${this.myString}</p>
      <ul>
        ${this.myArray.map(i => html`<li>${i}</li>`)}
      </ul>
      ${this.myBool?
      html`<p>Render some html if myBool is true</p>`:
      html`<p>Render some other html if myBool is false</p>`}
    `;
  }
}
