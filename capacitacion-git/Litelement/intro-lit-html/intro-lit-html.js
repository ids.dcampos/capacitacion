import { html, render } from "./node_modules/lit-html/lit-html";

const cadena =">>>>>Contenido Dinamico<<<<<";

const templateHolder =(paramString) => html `<h2>Contenido estatico (template) + ${paramString} </h2>`;
const objectTemplateResult = templateHolder(cadena);

render( templateHolder(cadena), document.getElementById('container1'));
render( templateHolder('otroooooooo textooooooo'), document.getElementById('container2'));