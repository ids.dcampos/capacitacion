import { html, LitElement } from 'lit';
import '../e-contact.js';


export class EContactList extends LitElement {

    static get properties(){
        return{
            contactos:{
                type: Array
            }
        };
    }

    constructor(){
        super();
        this.contactos =[
            {
                nombre: 'lucho godinez',
                email: 'user1-some-email@mail.com'
            },
            {
                nombre: 'hugo sanchez',
                email: 'user2-some-email@mail.com'
            },
            {
                nombre: 'gjo doe',
                email: 'user3-some-email@mail.com'
            }
        ];
    }

    render(){
        return html`
        <div>
            ${this.contactos.map(contact =>
                html`<e-contact
                    nombre="${contact.nombre}"
                    email="${contact.email}"></e-contact>`)}
        </div>
        

    `;
    }
}
