import {LitElement, html} from '../node_modules/lit-element/lit-element';

export class IntroLitelement extends LitElement{
    render(){
        return html`
        <p>Soy intro element</p>
        `;
    }
}

customElements.define('intro-litelement', IntroLitelement);