import { html } from 'lit';
import { fixture, expect } from '@open-wc/testing';

import '../litelements-openwc.js';

describe('LitelementsOpenwc', () => {
  it('has a default title "Hey there" and counter 5', async () => {
    const el = await fixture(html`<litelements-openwc></litelements-openwc>`);

    expect(el.title).to.equal('Hey there');
    expect(el.counter).to.equal(5);
  });

  it('increases the counter on button click', async () => {
    const el = await fixture(html`<litelements-openwc></litelements-openwc>`);
    el.shadowRoot.querySelector('button').click();

    expect(el.counter).to.equal(6);
  });

  it('can override the title via attribute', async () => {
    const el = await fixture(html`<litelements-openwc title="attribute title"></litelements-openwc>`);

    expect(el.title).to.equal('attribute title');
  });

  it('passes the a11y audit', async () => {
    const el = await fixture(html`<litelements-openwc></litelements-openwc>`);

    await expect(el).shadowDom.to.be.accessible();
  });
});
