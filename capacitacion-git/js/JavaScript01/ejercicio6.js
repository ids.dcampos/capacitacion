
a === 1 ? alert('hey, esto es un 1!') : alert('Raro, que puede ser?');
if(a === 1){
    alert('hey, esto es un 1');
}else alert('Raro, que puede ser?');

// no esta definida a como una variable, al no estarlo nos mostrara en consola error "a is not defined"
//Lo correcto es definir a:
// var a = 1;
// a === 1 ? alert('hey, esto es un 1!') : alert('Raro, que puede ser?');
// if(a === 1){
//       alert('hey, esto es un 1');
// }else alert('Raro, que puede ser?');
// Despues de estar definida nos mostrara dos alertas, primera con los operadores ternarios y ensegida con las estructuras de control
