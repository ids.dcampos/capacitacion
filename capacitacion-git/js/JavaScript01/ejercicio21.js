function foo(){
    var a = true;

    function bar(){
        var a = false;
        console.log(a);
    }
    const a = false;
    a = false;
    console.log(a);
}

/**  Identifier 'a' has already been declared */