function foo(){
    var a = 'hello';

    function bar(){
        var b = 'world';
        console.log(a);
        console.log(b);
    }
    console.log(a);
    console.log(b);
}
console.log(a);
console.log(b);

/** a is not defined */