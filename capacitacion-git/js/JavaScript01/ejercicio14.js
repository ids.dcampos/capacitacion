var a = 'hello' && '';
var b = '' && [];
var c = '' && undefined;
var d = 1 && 5;
var e = 0 && {};
var f = 0 && '' && 5;
var g = '' && 'yay' && 'boo';

console.log(a);
console.log(b);
console.log(c);
console.log(d);
console.log(e);
console.log(f);
console.log(g);

/**


5
0
0
 */