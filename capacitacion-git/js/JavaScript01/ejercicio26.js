var array = ['a', 'b', 'c', 'd', 'e', 'f'];

console.log(array.copyWithin(5,0,1));
console.log(array.copyWithin(3,0,3));
console.log(array.fill('Z',0,5));

var array = ['Alberto', 'Ana', 'Mauricio', 'Bernardo', 'Zoe'];
console.log(array.sort());
console.log(array.reverse());

/**
  'a', 'b', 'c', 'd', 'e', 'a' ]
[ 'a', 'b', 'c', 'a', 'b', 'c' ]
[ 'Z', 'Z', 'Z', 'Z', 'Z', 'c' ]
[ 'Alberto', 'Ana', 'Bernardo', 'Mauricio', 'Zoe' ]
[ 'Zoe', 'Mauricio', 'Bernardo', 'Ana', 'Alberto' ]
 */