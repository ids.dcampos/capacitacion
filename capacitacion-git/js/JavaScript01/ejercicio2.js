var animal = 'dog';
var result = (animal === 'kitty') ? 'cute' : 'still nice';
console.log(result);

//Las dos variables son comparadas con el operador === (Estrictamente iguales), si ambas variables son iguales pasara a una condicion ?: (Operador ternario if/else abreviado)
//Entonces si ambas variables son iguales en consola mostrara 'cute' y de lo contario no son iguales mostrara 'still nice'