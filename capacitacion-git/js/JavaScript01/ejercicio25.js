var array = [5, 10, 15, 20, 25];

Array.isArray(array);
array.includes(10);
array.includes(10, 2);
array.indexOf(25);
array.lastIndexOf(10, 0);

console.log(Array.isArray(array));
console.log(array.includes(10));
console.log(array.includes(10, 2));
console.log(array.indexOf(25));
console.log(array.lastIndexOf(10, 0));

/**
 true
true
false
4
-1
 */