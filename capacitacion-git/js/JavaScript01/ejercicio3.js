var animal = 'kitty';
var result = '';
if(animal === 'kitty'){
    result = 'cute';
}else{
    result = 'still nice';
}
console.log(result)

//Como vimos en el ejercicio anterior, realiza una comparcion, solo que aqui utilizamos las Estructuras de control if/else
//Y solo utilizamos el operador de comparacion === (Estrictamente iguales), lo que antes hicimos en 3 lineas de codigo, aqui lo realizamos en 8
//Vemos la importacion de utilizar los operadores de comparacion y los operadores ternarios