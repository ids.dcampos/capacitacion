function enPantalla(text){
    let element = document.getElementById('container');
    element.textContent = text;
}

function enconsola(text){
    console.log(text)
}

const req = new XMLHttpRequest();
req.addEventListener('load', () =>{
    if(req.status === 200){
        enconsola(req.responseText);
        enconsola(JSON.parse(req.responseText));
        enPantalla(req.responseText);
    }else{
        enconsola([req.status, req.statusText]);
    }
});

req.addEventListener('error', () => {
    enconsola('erro de red');
});

req.open('GET', 'https://jsonplaceholder.typicode.com/posts', true);
req.send(null);