const array1= [1,2,4,8]
const array2= [5,3,10,9]

const array3 = [...array1, ...array2]

array3.sort((a,b)=>{
    if(a==b){
        return 0;
    }
    if(a<b){
        return -1;
    }
    return 1;
})

console.log(array3)