class MiMensaje extends HTMLElement{
    constructor(){
        super();
        this.addEventListener('click', function(e){
            alert('Click en mensaje');
        });
        console.log('constructor: Cuando el elemento es creado');
    }

    static get observedAttributes(){
        return['msj'];
    }

    connectedCallback(){
        console.log('connectedCallback: cuando el elemento es insertado en el documento');
    }

    disconnectedCallback(){
        alert('disconnectedCallback: cuando el elemento es eliminado del documento');
    }

    adoptedCallback(){
        alert('adoptedCallback: Cuando el elemento es adoptado por otro documento');
    }

    /**Cuando un atributo es modificado, solo llamado en atributos observa
     * la propiedad observedAttributes
     */

    attributeChangedCallback(attrName, oldVal, newVal){
        console.log('attributeChangedCallback: Cuando cambia un atributo');
        if(attrName === 'msj'){
            this.pintarMensaje(newVal);
        }
        if(attrName === 'casi-visible'){
            this.setCasiVisible();
        }
    }

    /**propiedad casivisible sincronizada con el atributo casi-visible */

    get casiVisible(){
        return this.hasAttribute('casi-visible');
    }

    set casiVisible(value){
        if(value){
            this.setAttribute('casi-visible', '');
        }else{
            this.removeAttribute('casi-visible');
        }
    }

    /** define la opacidad del elemento basado en la propiedad casiVisible */

    setCasiVisible(){
        if(this.casiVisible){
            this.style.opacity=0.1;
        }else{
            this.style.opacity=1;
        }
    }

    static get observedAttributes(){
        return ['msj', 'casi-visible'];
    }

    pintarMensaje(msj){
        this.innerHTML = msj;
    }
    get msj(){
		return this.getAttribute('msj');
	}

	set msj(val){
		this.setAttribute('msj',val)
	}
}

customElements.define('mi-mensaje',MiMensaje);

let miMensaje = document.createElement('mi-mensaje');
miMensaje.msj = 'Otro mensaje';
document.body.appendChild(miMensaje);

let tercerMensaje = new MiMensaje();
tercerMensaje.msj = 'Tercer mensaje';
document.body.appendChild(tercerMensaje);