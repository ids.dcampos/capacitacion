class MiSaludo extends HTMLElement{
    constructor(){
        //obtengo la unica etiqueta 'template'
        const tpl = document.querySelector('template');
        //Clono su contenido y se crea una instancia del document fragment
        const tplInst = tpl.content.cloneNode(true);

        super(); //invoca el construtor de la clase padre
        //Se crea un chado dom para las instancias de mi-saludo
        this.attachShadow({mode:'open'});
        //Y se agrega el template dentro del shadow dow usando el elemento raiz shadowroot
        this.shadowRoot.appendChild(tplInst);
    }
}

//Se registra el custom element para poder ser utilizado declarativamanete en el html
//o imperativamente mediante js
customElements.define('mi-saludo', MiSaludo);